import typescript from "rollup-plugin-typescript2";
import clear from "rollup-plugin-clear";

export default {
  input: "src/index.ts",
  output: {
    file: "js_sdk/index.js",
    format: "esm",
  },
  plugins: [
    clear({
      targets: ["js_sdk"],
    }),
    typescript(),
  ],
  watch: {
    include: "src/**",
  },
};
