import {
  ActionSheet,
  ActionSheetConfig,
  Configs,
  LoadingConfig,
  ModalConfig,
  ToastConfig,
} from "./typings";
import { debounce } from "lodash";

/**
 * 默认toast配置
 */
let defaultToastConfig: ToastConfig = {
  duration: 1500,
  icon: "none",
  mask: true,
  success: () => {},
  fail: () => {},
  complete: () => {},
};

/**
 * 防抖时间
 */
const shakeTiming = 200;

/**
 * 设置全局toast默认参数
 * @param setConfig
 */
export function setToastConfig(setConfig: ToastConfig) {
  defaultToastConfig = generateConfig(defaultToastConfig, setConfig);
}

/**
 * 显示消息提示框
 * @param msg 消息
 * @param customConfig 自定义配置
 * @returns
 */
export function showToast(msg: string = "", customConfig?: ToastConfig) {
  uni.hideToast();

  const config = generateConfig(defaultToastConfig, customConfig);

  return new Promise((rs, rj) => {
    uni.showToast({
      ...config,
      title: msg,
      success: () => {
        config.success();
      },
      fail: function () {
        config.fail();
        rj();
      },
      complete: function () {
        config.complete();
      },
    });

    setTimeout(() => {
      rs(null);
    }, config.duration + shakeTiming);
  });
}

/**
 * 默认loading配置
 */
let defaultLoadingConfig: LoadingConfig = {
  mask: true,
  success: () => {},
  fail: () => {},
  complete: () => {},
};

/**
 * 设置全局loading默认参数
 * @param setConfig
 */
export function setLoadingConfig(setConfig: LoadingConfig) {
  defaultLoadingConfig = generateConfig(defaultLoadingConfig, setConfig);
}

/**
 * 显示 loading 提示框, 需主动调用 uni.hideLoading 才能关闭提示框
 * @param msg 消息文字
 * @param customConfig 自定义配置
 * @returns
 */
export function showLoading(msg: string = "", customConfig?: LoadingConfig) {
  const config = generateConfig(defaultLoadingConfig, customConfig);
  return uni.showLoading({
    ...config,
    title: msg,
    success: () => {
      config.success();
    },
    fail: () => {
      config.fail();
    },
    complete: () => {
      config.complete();
    },
  });
}

const hideUniLoading = debounce(uni.hideLoading, 400);

/**
 * 隐藏loading，自带400ms防抖延迟
 */
export function hideLoading() {
  return hideUniLoading();
}

/**
 * 默认modal配置
 */
let defaultModalConfig: ModalConfig = {
  title: "提示",
  success: () => {},
  fail: () => {},
  complete: () => {},
};

/**
 * 设置全局modal配置
 * @param setConfig
 */
export function setModalConfig(setConfig: ModalConfig) {
  defaultModalConfig = generateConfig(defaultModalConfig, setConfig);
}

/**
 * 显示模态弹窗，可以只有一个确定按钮，也可以同时有确定和取消按钮
 * @param content 提示消息
 * @param customConfig 自定义配置
 * @returns
 */
export function showModal(content: string = "", customConfig?: ModalConfig) {
  const config = generateConfig(defaultModalConfig, customConfig);
  const {
    title,
    showCancel,
    cancelText,
    cancelColor,
    confirmText,
    confirmColor,
  } = config;

  return new Promise((rs, rj) => {
    uni.showModal({
      title,
      showCancel,
      cancelText,
      cancelColor,
      confirmText,
      confirmColor,
      content,
      success: (res) => {
        config.success();
        if (res.confirm) rs(true);
        else if (res.cancel) rj(false);
      },
      fail: () => {
        config.fail();
        rj(false);
      },
      complete: () => {
        config.complete();
      },
    });
  });
}

/**
 * 默认actionSheet配置
 */
let defaultActionSheetConfig: ActionSheetConfig = {
  success: () => {},
  fail: () => {},
  complete: () => {},
};

/**
 * 设置全局actionSheet配置
 * @param setConfig
 */
export function setActionSheetConfig(setConfig: ActionSheetConfig) {
  defaultActionSheetConfig = generateConfig(
    defaultActionSheetConfig,
    setConfig
  );
}

/**
 * 从底部向上弹出操作菜单
 * @param actions 操作菜单数组
 * @param customConfig 自定义配置
 * @returns
 */
export function showActionSheet(
  actions: ActionSheet[] = [],
  customConfig?: ActionSheetConfig
) {
  const config = generateConfig(defaultActionSheetConfig, customConfig);
  const itemList = actions.map((i) => i.label);
  const { itemColor, popover } = config;
  return uni.showActionSheet({
    itemList,
    itemColor,
    // @ts-ignore
    popover,
    success: ({ tapIndex }) => {
      actions[tapIndex].command(tapIndex);
    },
    fail: () => {
      config.fail();
    },
    complete: () => {
      config.complete();
    },
  });
}

/**
 * 构造配置
 * @param customConfig 自定义配置
 * @returns
 */
function generateConfig<T = Configs>(defaultConfig: T, customConfig: T) {
  const config = {
    ...defaultConfig,
    ...customConfig,
  };
  return config;
}
